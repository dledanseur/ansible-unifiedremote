# ansible-unifiedremote

Installs Unified remote https://unifiedremote.com on an ubuntu server.

Usage:

```
#!yaml

- hosts: my.host.example.com
  roles:
   - dledanseur.unifiedremote
     vars:
       unifiedremote_download_url: https://www.unifiedremote.com/download/linux-x64-deb # optional
       unifiedremote_autostart_user: auser # optional, default to none

```


By using the optional unifiedremote_autostart_user, the playbook will install the unifiedremote at startup of the given user's session. Note that running the unified remote server once does actually the same.


